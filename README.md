# BASDAT

# Deployment di Server Fasikom UI

Server Fasilkom yang digunakan untuk deploy Tugas Kelompok kalian yang berbasis Django dapat menggunakan server yang sudah kami setup dengan IP address **152.118.25.3**. 
Kekurangan server ini adalah **hanya dapat diakses di lingkungan kampus**.
Jika aplikasi yang Anda buat tidak berbasis framework apapun, Anda dapat menggunakan server **kawung.cs.ui.ac.id** dengan panduan seperti yang sudah dipost di Scele. 

## Terhubung ke server **152.118.25.3**
Anda tidak dapat menggunakan WinSCP atau sejenisnya untuk mengakses IP Address ini

1. Buka Putty kemudian akses ke **kawung.cs.ui.ac.id** dengan port **22**
2. Login menggunakan akun SSO Anda seperti biasa
3. Login ke **152.118.25.3** dengan eksekusi perintah:`ssh [username]@152.118.25.3 -l [usernamesso]`
sebagai contoh: ssh iisafriyanti@152.118.25.3 -l iisafriyanti
4. Masukkan password sso Anda
5. **Selamat, Anda sudah masuk ke dalam server**

## Environment di 152.118.25.3
1. Git <br/>
Setup proxy: ``git config --global http.proxy http://proxy.cs.ui.ac.id:8080/``
2. Python 3.5.3 <br/>
Jika tidak percaya, cek: ``python3 --version``. Dengan demikian, gunakan ``python3`` untuk mengeksekusi perintah-perintah python.
3. PHP 7 <br/>
Jika tidak percaya, cek: ``php --version``

## Tahap deployment Django di **152.118.25.3** dan terhubung ke dbpg.cs.ui.ac.id
Kami sudah membuatkan contoh Django app yang sudah terhubung ke database dbpg.cs.ui.ac.id dan akan saya gunakan sebagai contoh
1. Silakan clone repository Anda yang ada di Gitlab maupun Github.
Contoh: ``git clone https://gitlab.com/iisafriyanti/basdat.git``
2. Buatlah virtual environment di dalam folder yang sudah Anda clone dengan perintah `python3 -m venv env`<br/>
Mohon untuk membuat virtual environment terbaru jika Anda bekerja di server atau di local
3. Aktifkan virtual environment: ``source env/bin/activate``
4. Set up proxy: ``export all_proxy="http://proxy.cs.ui.ac.id:8080/"``
5. Install requirements untuk mendeploy Django: ``pip3 install -r requirements.txt``
6. Terhubung ke dbpg.cs.ui.ac.id dengan mengubah pengaturan pada `settings.py`

    ```python
    DATABASES = {
    'default': {
      'ENGINE': 'django.db.backends.postgresql',
      'NAME': '[dbname]',
      'USER': '[username]',
      'PASSWORD': '[password]',
      'HOST': 'dbpg.cs.ui.ac.id',
      'PORT': '',
     }
    }
    ```
 Untuk Tugas Kelompok Basdat, dbname dan username adalah sama seperti yang sudah diumumkan di Scele.
7. Jika ingin membuat user admin: `python3 manage.py createsuperuser`
8. Lakukan migrations `python3 manage.py migrate [namaapp]`, contoh repository ini `python3 manage.py migrate blog`

9. Jalankan aplikasi ``nohup python3 manage.py runserver 152.118.25.3:[PORTNO]``
10. Eureka! Buka browser Anda kemudian akses **152.118.25.3:[PORTNO]/admin**
11. Buka dbpg.cs.ui.ac.id dan cek skema relasi di database Anda, pasti sudah terisi

## Referensi
How to use PostgreSQL with your Django Application on Ubuntu [https://www.digitalocean.com/community/tutorials/how-to-use-postgresql-with-your-django-application-on-ubuntu-14-04]
  
