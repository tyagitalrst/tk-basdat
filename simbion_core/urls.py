from django.conf.urls import url
from .views import index, simbion_register, simbion_login, simbion_listbeasiswa,simbion_detailbeasiswa, simbion_detailbeasiswamahasiswa, simbion_formdaftarmahasiwa, simbion_listpendaftar
from .views import simbion_buatbeasiswa, simbion_beasiswadonatur, base_homepage_donatur, base_homepage_mahasiswa
from .views import simbion_beasiswadaripaket, simbion_paketbeasiswabaru, simbion_home, simbion_logout
from .views import register_mahasiswa, register_individual, register_yayasan, login


urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^register', simbion_register, name='simbion_register'),
    url(r'^login', login, name='login'),
    url(r'^submit-login', simbion_login, name='simbion_login'),
    url(r'^logout', simbion_logout, name='simbion_logout'),
    url(r'^list-beasiswa', simbion_listbeasiswa, name='simbion_listbeasiswa'),
    url(r'^detail-beasiswa', simbion_detailbeasiswa, name='simbion_detailbeasiswa'),
    url(r'^daftar-beasiswa', simbion_detailbeasiswamahasiswa, name='simbion_detailbeasiswamahasiswa'),
    url(r'^form-pendaftaran-beasiswa', simbion_formdaftarmahasiwa, name='simbion_formdaftarmahasiswa'),
    url(r'^beasiswa-paket', simbion_beasiswadaripaket, name='simbion_beasiswadaripaket'),
    url(r'^beasiswa-baru', simbion_paketbeasiswabaru, name='simbion_paketbeasiswabaru'),
    url(r'^buat-beasiswa', simbion_buatbeasiswa, name='simbion_buatbeasiswa'),
    url(r'^beasiswa-donatur', simbion_beasiswadonatur, name='simbion_beasiswadonatur'),
    url(r'^list-pendaftar', simbion_listpendaftar, name='simbion_listpendaftar'),
    url(r'^home', simbion_home, name='simbion_home'),
    url(r'^homepage-mahasiswa', base_homepage_mahasiswa, name='base_homepage_mahasiswa'),
    url(r'^homepage-donatur', base_homepage_donatur, name='base_homepage_donatur'),
    url(r'^mahasiswa', register_mahasiswa, name='register_mahasiswa'),
    url(r'^individual', register_individual, name='register_individual'),
    url(r'^yayasan', register_yayasan, name='register_yayasan'),
]