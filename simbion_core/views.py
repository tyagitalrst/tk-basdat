from django.shortcuts import render, redirect, reverse
from django.db import connection, transaction
from django.http.response import HttpResponseRedirect
from django.contrib import messages

# Create your views here.

response = {}
def index(request):
    return render(request, 'simbion_homepage.html')

def simbion_register(request):
    if ('username' in request.session.keys()):
        return HttpResponseRedirect(reverse('core:simbion_home'))
    return render(request, 'simbion_register.html')

def register_mahasiswa(request):
    if(request.method == "POST"):
        username = request.POST.get('username')
        password = request.POST.get('password')
        npm = request.POST.get('npm')
        email = request.POST.get('email')
        nama = request.POST.get('nama')
        notelp = request.POST.get('notelp')
        alamat_tinggal = request.POST.get('alamat_tinggal')
        alamat_domisili = request.POST.get('alamat_domisili')
        bank = request.POST.get('bank')
        no_rek = request.POST.get('no_rek')
        nama_pemilik = request.POST.get('nama_pemilik')

        cursor = connection.cursor()
        cursor.execute("SELECT * FROM pengguna WHERE username = %s", [username])
        match = cursor.fetchone();
        if(match):
            if len(match) > 0:
                return render(request, 'simbion_fail_register.html')

        cursor = connection.cursor()
        cursor.execute("SELECT * FROM mahasiswa WHERE npm = %s", [npm])
        match = cursor.fetchone();
        if(match):
            if len(match) > 0:
                return render(request, 'simbion_fail_register.html')

        query = "INSERT INTO PENGGUNA(username, password, role) VALUES (%s, %s, 'mahasiswa')"
        cursor.execute(query, [username, password])

        query = "INSERT INTO MAHASISWA(npm, email, nama, no_telp, alamat_tinggal, alamat_domisili, nama_bank, no_rekening, nama_pemilik, username) " \
                           "VALUES (%s, %s, %s, %s, %s, %s, %s, %s,%s, %s)"
        cursor.execute(query, [npm, email, nama, notelp, alamat_tinggal, alamat_domisili, bank, no_rek, nama_pemilik, username])

        request.session['username'] = username
        request.session['role'] = 'mahasiswa'

        return render(request, 'simbion_success_register.html')


def register_individual(request):
    if (request.method == "POST"):
        username = request.POST.get('usernameIn')
        password = request.POST.get('passwordIn')
        no_id = request.POST.get('no_idIn')
        nik = request.POST.get('nikIn')
        email = request.POST.get('emailIn')
        nama = request.POST.get('namaIn')
        npwp = request.POST.get('npwpIn')
        notelp = request.POST.get('notelpIn')
        alamat = request.POST.get('alamatIn')

        cursor = connection.cursor()
        cursor.execute("SELECT * FROM pengguna WHERE username = %s", [username])
        match = cursor.fetchone();
        if(match):
            if len(match)>0:
                return render(request, 'simbion_fail_register.html')

        cursor = connection.cursor()
        cursor.execute("SELECT * FROM donatur WHERE nomor_identitas = %s", [no_id])
        match = cursor.fetchone();
        if(match):
            if len(match)>0:
                return render(request, 'simbion_fail_register.html')

        cursor = connection.cursor()
        cursor.execute("SELECT * FROM individual_donor WHERE nik = %s", [nik])
        match = cursor.fetchone();
        if(match):
            if len(match)>0:
                return render(request, 'simbion_fail_register.html')

        query = "INSERT INTO PENGGUNA(username, password, role) VALUES (%s, %s, 'donatur')"
        cursor.execute(query, [username, password])


        query = "INSERT INTO donatur(nomor_identitas, email, nama, npwp, no_telp, alamat, username) " \
                         "VALUES (%s, %s, %s, %s, %s, %s, %s)"
        cursor.execute(query, [no_id, email, nama, npwp, notelp, alamat, username])


        query = "INSERT INTO INDIVIDUAL_DONOR(nik, nomor_identitas_donatur) VALUES (%s, %s)"
        cursor.execute(query, [nik, no_id])


        request.session['username'] = username
        request.session['role'] = 'donatur'

        return render(request, 'simbion_success_register.html')

def register_yayasan(request):
    if (request.method == "POST"):
        username = request.POST.get('usernameYa')
        password = request.POST.get('passwordYa')
        no_id = request.POST.get('no_idYa')
        no_sk = request.POST.get('no_skYa')
        email = request.POST.get('emailYa')
        nama = request.POST.get('namaYa')
        notelp = request.POST.get('notelpYa')
        npwp = request.POST.get('npwpYa')
        alamat = request.POST.get('alamatYa')

        cursor = connection.cursor()
        cursor.execute("SELECT * FROM pengguna WHERE username = %s", [username])
        match = cursor.fetchone();
        if(match):
            if len(match)>0:
                return render(request, 'simbion_fail_register.html')

        cursor = connection.cursor()
        cursor.execute("SELECT * FROM donatur WHERE nomor_identitas = %s", [no_id])
        match = cursor.fetchone();
        if(match):
            if len(match)>0:
                return render(request, 'simbion_fail_register.html')

        cursor = connection.cursor()
        cursor.execute("SELECT * FROM yayasan WHERE no_sk_yayasan = %s", [no_sk])
        match = cursor.fetchone();
        if(match):
            if len(match)>0:
                return render(request, 'simbion_fail_register.html')

        query = "INSERT INTO PENGGUNA(username, password, role) VALUES (%s, %s, 'donatur')"
        cursor.execute(query, [username, password])

        query = "INSERT INTO donatur(nomor_identitas, email, nama, npwp, no_telp, alamat, username) " \
                         "VALUES (%s, %s, %s, %s, %s, %s, %s)"
        cursor.execute(query, [no_id, email, nama, npwp, notelp, alamat, username])

        query = "INSERT INTO YAYASAN(no_sk_yayasan, email, nama, no_telp_cp, nomor_identitas_donatur) " \
                           "VALUES (%s, %s, %s, %s, %s, %s)"
        cursor.execute(query, [no_sk, email, nama, notelp, no_id])

        request.session['username'] = username
        request.session['role'] = 'donatur'

        return render(request, 'simbion_success_register.html')


def simbion_login(request):
    username = request.POST.get('username')
    password = request.POST.get('password')
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM pengguna WHERE username= %s", [username])
    match = cursor.fetchone();
    if (match):
        cursor.execute("SELECT password FROM pengguna WHERE username='" + username + "'")
        match = cursor.fetchone();
        if (match[0] == password):
            request.session['username'] = username
            cursor.execute("SELECT role FROM pengguna WHERE username='" + username + "'")
            match = cursor.fetchone();
            request.session['role'] = match[0]
            return HttpResponseRedirect(reverse('core:simbion_home'))
    messages.error(request, "Username atau Password yang anda masukkan salah")
    return HttpResponseRedirect(reverse('core:login'))

def simbion_home(request):
    if ('username' in request.session.keys()):
        role = request.session['role']
        if (role != "mahasiswa"):
            return render(request, 'layout/base_homepage_donatur.html')
        return render(request, 'layout/base_homepage_mahasiswa.html')
    else:
        return HttpResponseRedirect(reverse('core:simbion_login'))

def login(request):
    if ('username' in request.session.keys()):
        return HttpResponseRedirect(reverse('core:simbion_home'))
    return render(request, 'simbion_login.html')

def simbion_logout(request):
    request.session.flush()
    return HttpResponseRedirect(reverse('core:index'))

def simbion_listbeasiswa(request):
    return render(request, 'simbion_success_daftar.html')

def simbion_listpendaftar(request):
    cursor = connection.cursor()
    response['username'] = request.session['username']

    cursor.execute(" SELECT distinct nama, deskripsi, kode FROM skema_beasiswa")
    response['detail_beasiswa'] = cursor.fetchone()

    cursor.execute("SELECT no_urut, nama, npm, waktu_daftar, status_terima FROM pendaftaran NATURAL JOIN mahasiswa")
    response['array_pendaftar'] = cursor.fetchall()

    return render(request, 'simbion_listpendaftar.html', response)

def simbion_listpendaftar_terima(request, no_urut, kode_beasiswa, npm):
    cursor = connection.cursor()
    cursor.execute("UPDATE pendaftaran SET status_terima='DITERIMA' WHERE no_urut='" + no_urut + "' AND kode_skema_beasiswa='" + kode_beasiswa + "' AND npm='" + npm + "'")

    return detail(request, kode_beasiswa)

def simbion_listpendaftar_tolak(request, no_urut, kode_beasiswa, npm):
    cursor = connection.cursor()
    cursor.execute("UPDATE pendaftaran SET status_terima='DITOLAK' WHERE no_urut='" + no_urut + "' AND kode_skema_beasiswa='" + kode_beasiswa + "' AND npm='" + npm + "'")

    return detail(request, kode_beasiswa)

def simbion_detailbeasiswa(request):
    return render(request, 'simbion_detailbeasiswa.html')

def simbion_detailbeasiswamahasiswa(request):
    return render(request, 'simbion_detailbeasiswamahasiswa.html')

def simbion_formdaftarmahasiwa(request):
    cursor = connection.cursor()

    cursor.execute("SELECT kode, nama, tgl_mulai_pendaftaran from skema_beasiswa_aktif JOIN skema_beasiswa ON kode_skema_beasiswa = kode WHERE status = 'TRUE' ORDER BY kode")
    response['array_beasiswa_aktif'] = cursor.fetchall()

    return render(request, 'simbion_formdaftarmahasiswa.html', response)

def simbion_buatbeasiswa(request):
    return render(request, 'simbion_buatbeasiswa.html')

def simbion_beasiswadonatur(request):
    cursor = connection.cursor()
    response['username'] = request.session['username']

    cursor.execute("SELECT nomor_identitas FROM donatur WHERE username='" + response['username']+"'")
    response['nomor_identitas'] = cursor.fetchone()[0]

    cursor.execute(" SELECT distinct nama, tgl_tutup_pendaftaran, status, jumlah_pendaftar, kode FROM skema_beasiswa join skema_beasiswa_aktif on kode=kode_skema_beasiswa WHERE nomor_identitas_donatur='" + response['nomor_identitas']+"'")
    output = cursor.fetchall()
    response['array_beasiswa_donatur'] = output

    return render(request, 'simbion_beasiswadonatur.html', response)

def simbion_beasiswadaripaket(request):
    if ('kode' in request.session.keys()):
        return HttpResponseRedirect(reverse('core:simbion_home'))
    return render(request, 'simbion_beasiswadaripaket.html')

    if(request.method == "POST"):
        kode = request.POST.get('kodeYa')
        nomor = request.POST.get('nomorYa')
        tgl_mulai = request.POST.get('mulaiYa')
        tgl_tutup = request.POST.get('tutupYa')

        cursor = connection.cursor()
        cursor.execute("SELECT * FROM skema_beasiswa WHERE kode = %s", [kode])
        match = cursor.fetchone();
        if(match):
            if len(match)>0:
                return render(request, 'simbion_fail_register.html')

        cursor = connection.cursor()
        cursor.execute("SELECT * FROM skema_beasiswa_aktif WHERE no_urut = %s", [nomor])
        match = cursor.fetchone();
        if(match):
            if len(match)>0:
                return render(request, 'simbion_fail_register.html')

        query = "INSERT INTO skema_beasiswa(kode) VALUES (%s)"
        cursor.execute(query, [kode])

        query = "INSERT INTO skema_beasiswa_aktif(kode_skema_beasiswa, no_urut, tgl_mulai_pendaftaran, tgl_tutup_pendaftaran) VALUES (%s, %s, %s, %s)"
        cursor.execute(query, [kode, nama, tgl_mulai, tgl_tutup])

        request.sesion['kode'] = kode
        request.sesion['no_urut'] = nomor

        return render(request, 'simbion_success_daftar.html')

def simbion_paketbeasiswabaru(request):
    if ('kode' in request.session.keys()):
        return HttpResponseRedirect(reverse('core:simbion_home'))
    return render(request, 'simbion_paketbeasiswabaru.html')

    if(request.method == "POST"):
        kode = request.POST.get('kode')
        nama = request.POST.get('nama')
        jenis = request.POST.get('jenis')
        deskripsi = request.POST.get('deskripsi')
        nomor = request.POST.get('nomor')
        syarat = request.POST.get('syarat')

        cursor = connection.cursor()
        cursor.execute("SELECT * from skema_beasiswa where kode = %s", [kode])
        match = cursor.fetchone();
        if(match):
            if len(match)>0:
                return render(request, 'simbion_fail_register.html')

        query = "INSERT INTO SKEMA_BEASISWA(kode, nama, jenis, deskripsi, nomor_identitas_donatur) VALUES (%s,%s,%s,%s,%s)"
        cursor.execute(query, [kode, nama, jenis, deskripsi, nomor])
        query = "INSERT INTO SYARAT_BEASISWA(kode_beasiswa, syarat) VALUES (%s,%s)"
        cursor.execute(query, [kode, syarat])

        request.sesion['kode'] = kode
        return render(request, 'simbion_success_daftar.html')

def base_homepage_mahasiswa(request):
    return render(request, 'layout/base_homepage_mahasiswa.html')

def base_homepage_donatur(request):
    return render(request, 'layout/base_homepage_donatur.html')