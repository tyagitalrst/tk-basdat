from django.apps import AppConfig


class SimbionCoreConfig(AppConfig):
    name = 'simbion_core'
