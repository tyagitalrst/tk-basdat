# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class Admin(models.Model):
    username = models.OneToOneField('Pengguna', db_column='username', on_delete=models.CASCADE)

    class Meta:
        managed = False
        db_table = 'admin'


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class Donatur(models.Model):
    nomor_identitas = models.CharField(primary_key=True, max_length=20)
    email = models.EmailField(max_length=50)
    nama = models.CharField(max_length=50)
    npwp = models.CharField(max_length=20)
    no_telp = models.CharField(max_length=20, blank=True, null=True)
    alamat = models.CharField(max_length=50)
    username = models.ForeignKey('Pengguna', db_column='username', on_delete=models.CASCADE)

    class Meta:
        managed = False
        db_table = 'donatur'


class IndividualDonor(models.Model):
    nik = models.CharField(primary_key=True, max_length=16)
    nomor_identitas_donatur = models.ForeignKey(Donatur, db_column='nomor_identitas_donatur', on_delete=models.CASCADE)

    class Meta:
        managed = False
        db_table = 'individual_donor'


class Mahasiswa(models.Model):
    npm = models.CharField(primary_key=True, max_length=20)
    email = models.EmailField(max_length=50)
    nama = models.CharField(max_length=50)
    no_telp = models.CharField(max_length=20, blank=True, null=True)
    alamat_tinggal = models.CharField(max_length=50)
    alamat_domisili = models.CharField(max_length=50)
    nama_bank = models.CharField(max_length=50)
    no_rekening = models.CharField(unique=True, max_length=20)
    nama_pemilik = models.CharField(max_length=20)
    username = models.ForeignKey('Pengguna', db_column='username', on_delete=models.CASCADE)

    class Meta:
        managed = False
        db_table = 'mahasiswa'


class Pembayaran(models.Model):
    urutan = models.IntegerField(primary_key=True)
    kode_skema_beasiswa = models.ForeignKey('SkemaBeasiswaAktif', db_column='kode_skema_beasiswa', on_delete=models.CASCADE)
    no_urut_skema_beasiswa_aktif = models.IntegerField()
    npm = models.ForeignKey(Mahasiswa, db_column='npm', on_delete=models.CASCADE)
    keterangan = models.CharField(max_length=50)
    tgl_bayar = models.DateField()
    nominal = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'pembayaran'
        unique_together = (('urutan', 'kode_skema_beasiswa', 'no_urut_skema_beasiswa_aktif', 'npm'),)


class Pendaftaran(models.Model):
    no_urut = models.OneToOneField('SkemaBeasiswaAktif', db_column='no_urut', on_delete=models.CASCADE)
    kode_skema_beasiswa = models.IntegerField()
    npm = models.ForeignKey(Mahasiswa, models.DO_NOTHING, db_column='npm')
    waktu_daftar = models.DateTimeField()
    status_daftar = models.CharField(max_length=20)
    status_terima = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'pendaftaran'
        unique_together = (('no_urut', 'kode_skema_beasiswa', 'npm'),)


class Pengguna(models.Model):
    username = models.CharField(primary_key=True, max_length=20)
    password = models.CharField(max_length=20)
    role = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'pengguna'


class Pengumuman(models.Model):
    tanggal = models.DateField(primary_key=True)
    no_urut_skema_beasiswa_aktif = models.ForeignKey('SkemaBeasiswaAktif', db_column='no_urut_skema_beasiswa_aktif', on_delete=models.CASCADE)
    kode_skema_beasiswa = models.IntegerField()
    username = models.ForeignKey(Pengguna, db_column='username', on_delete=models.CASCADE)
    judul = models.CharField(max_length=20)
    isi = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'pengumuman'
        unique_together = (('tanggal', 'no_urut_skema_beasiswa_aktif', 'kode_skema_beasiswa', 'username'),)


class RiwayatAkademik(models.Model):
    no_urut = models.IntegerField(primary_key=True)
    npm = models.ForeignKey(Mahasiswa, db_column='npm', on_delete=models.CASCADE)
    semester = models.CharField(max_length=1)
    tahun_ajaran = models.CharField(max_length=9)
    jumlah_sks = models.IntegerField()
    ips = models.FloatField()
    lampiran = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'riwayat_akademik'
        unique_together = (('no_urut', 'npm'),)


class SkemaBeasiswa(models.Model):
    kode = models.IntegerField(primary_key=True)
    nama = models.CharField(max_length=50)
    jenis = models.CharField(max_length=20)
    deskripsi = models.CharField(max_length=50)
    nomor_identitas_donatur = models.ForeignKey(Donatur, db_column='nomor_identitas_donatur', on_delete=models.CASCADE)

    class Meta:
        managed = False
        db_table = 'skema_beasiswa'


class SkemaBeasiswaAktif(models.Model):
    kode_skema_beasiswa = models.OneToOneField(SkemaBeasiswa, db_column='kode_skema_beasiswa', on_delete=models.CASCADE)
    no_urut = models.IntegerField()
    tgl_mulai_pendaftaran = models.DateField()
    tgl_tutup_pendaftaran = models.DateField()
    periode_penerimaan = models.CharField(max_length=50)
    status = models.CharField(max_length=20)
    jumlah_pendaftar = models.IntegerField()
    total_pembayaran = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'skema_beasiswa_aktif'
        unique_together = (('kode_skema_beasiswa', 'no_urut'),)


class SyaratBeasiswa(models.Model):
    kode_beasiswa = models.OneToOneField(SkemaBeasiswa, db_column='kode_beasiswa', on_delete=models.CASCADE)
    syarat = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'syarat_beasiswa'
        unique_together = (('kode_beasiswa', 'syarat'),)


class TempatWawancara(models.Model):
    kode = models.IntegerField(primary_key=True)
    nama = models.CharField(max_length=50)
    lokasi = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'tempat_wawancara'


class Wawancara(models.Model):
    no_urut_skema_beasiswa_aktif = models.OneToOneField(SkemaBeasiswaAktif, db_column='no_urut_skema_beasiswa_aktif', on_delete=models.CASCADE)
    kode_skema_beasiswa = models.IntegerField()
    jadwal = models.DateTimeField()
    kode_tempat_wawancara = models.ForeignKey(TempatWawancara, db_column='kode_tempat_wawancara', on_delete=models.CASCADE)

    class Meta:
        managed = False
        db_table = 'wawancara'
        unique_together = (('no_urut_skema_beasiswa_aktif', 'kode_skema_beasiswa', 'jadwal'),)


class Yayasan(models.Model):
    no_sk_yayasan = models.CharField(primary_key=True, max_length=20)
    email = models.EmailField(max_length=50)
    nama = models.CharField(max_length=50)
    no_telp_cp = models.CharField(max_length=20, blank=True, null=True)
    nomor_identitas_donatur = models.ForeignKey(Donatur, db_column='nomor_identitas_donatur', on_delete=models.CASCADE)

    class Meta:
        managed = False
        db_table = 'yayasan'
